const {
    configureWebpack,
    graphQL: { getMediaURL, getUnionAndInterfaceTypes }
} = require('@magento/pwa-buildpack');
const { DefinePlugin } = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = async env => {
    const mediaUrl = await getMediaURL();

    global.MAGENTO_MEDIA_BACKEND_URL = mediaUrl;

    const unionAndInterfaceTypes = await getUnionAndInterfaceTypes();

    const { clientConfig, serviceWorkerConfig } = await configureWebpack({
        context: __dirname,
        vendor: [
            '@apollo/react-hooks',
            'apollo-cache-inmemory',
            'apollo-cache-persist',
            'apollo-client',
            'apollo-link-context',
            'apollo-link-http',
            'informed',
            'react',
            'react-dom',
            'react-feather',
            'react-redux',
            'react-router-dom',
            'redux',
            'redux-actions',
            'redux-thunk'
        ],
        special: {
            'react-feather': {
                esModules: true
            },
            '@magento/peregrine': {
                esModules: true,
                cssModules: true
            },
            '@magento/venia-ui': {
                cssModules: true,
                esModules: true,
                graphqlQueries: true,
                rootComponents: true,
                upward: true
            }
        },
        env
    });

    /**
     * configureWebpack() returns a regular Webpack configuration object.
     * You can customize the build by mutating the object here, as in
     * this example. Since it's a regular Webpack configuration, the object
     * supports the `module.noParse` option in Webpack, documented here:
     * https://webpack.js.org/configuration/module/#modulenoparse
     */
    clientConfig.module.noParse = [/braintree\-web\-drop\-in/];
    clientConfig.plugins = [
        ...clientConfig.plugins,
        new DefinePlugin({
            /**
             * Make sure to add the same constants to
             * the globals object in jest.config.js.
             */
            UNION_AND_INTERFACE_TYPES: JSON.stringify(unionAndInterfaceTypes),
            STORE_NAME: JSON.stringify('Venia')
        }),
        new HTMLWebpackPlugin({
            filename: 'index.html',
            template: './template.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true
            }
        })
    ];

    /**
     * Extend default from venia-concept
     */
    const path = require('path');
    // Plugin to override components
    const NormalModuleOverridePlugin = require('./src/lib/plugins/normalModuleOverrideWebpackPlugin');
    // Overwrite components mapping
    const parentPackagePath = path.resolve('./node_modules/@magento/venia-ui');
    const componentOverrideMapping = { 
        [`${parentPackagePath}/lib/components/Footer/footer.js`]: 'src/lib/components/Footer/footer.js',
        [`${parentPackagePath}/lib/components/Header/header.js`]: 'src/lib/components/Header/header.js',
        [`${parentPackagePath}/lib/components/Routes/routes.js`]: 'src/lib/components/Routes/routes.js',
        [`${parentPackagePath}/lib/components/MiniCart/footer.js`]: 'src/lib/components/MiniCart/footer.js',
    };
    // Push the override plugin
    clientConfig.plugins.push(
    new NormalModuleOverridePlugin(componentOverrideMapping)
    );
   
    return [clientConfig, serviceWorkerConfig];
};
