import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';
import { RetryLink } from 'apollo-link-retry';

import { Util } from '@magento/peregrine';
import { Adapter } from '@magento/venia-drivers';
import store from './store';
import app from '@magento/peregrine/lib/store/actions/app';
import App, { AppContextProvider } from '@magento/venia-ui/lib/components/App';

import { registerSW } from './registerSW';
import './index.css';

const { BrowserPersistence } = Util;
const apiBase = new URL('/graphql', location.origin).toString();
import { InMemoryCache, IntrospectionFragmentMatcher } from "apollo-cache-inmemory";
import { cacheKeyFromType } from '@magento/venia-ui/lib/util/apolloCache';
// 
const countryCurrency = setContext((_, { headers }) => {
    // setup country currency 
    const storage = new BrowserPersistence();
    const currency = storage.getItem('currency');
    const eswCountry = storage.getItem('eswCountry');
    const store = storage.getItem('store');
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            'ESW-COUNTRY': eswCountry ? `${eswCountry}` : '',
            'Content-Currency': currency ? `${currency}` : '', 
            'Store': store ? `${store}` : '', 
        }
    };
});

/**
 * The Venia adapter provides basic context objects: a router, a store, a
 * GraphQL client, and some common functions.
 */

// The Venia adapter is not opinionated about auth.
const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists.
    const storage = new BrowserPersistence();
    const token = storage.getItem('signin_token');

    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : ''
        }
    };
});

// @see https://www.apollographql.com/docs/link/composition/.
const apolloLink = ApolloLink.from([
    // by default, RetryLink will retry an operation five (5) times.
    new RetryLink(),
    authLink,
    countryCurrency,
    // An apollo-link-http Link
    Adapter.apolloLink(apiBase)
]);

const fragmentMatcher = new IntrospectionFragmentMatcher({
    // UNION_AND_INTERFACE_TYPES is injected into the bundle by webpack at build time.
    introspectionQueryResultData: UNION_AND_INTERFACE_TYPES
});
const apolloCache = new InMemoryCache({
    dataIdFromObject: cacheKeyFromType,
    fragmentMatcher
});

ReactDOM.render(
    <Adapter apiBase={apiBase} apollo={{ link: apolloLink, cache: apolloCache}} store={store}>
        <AppContextProvider>
            <App />
        </AppContextProvider>
    </Adapter>,
    document.getElementById('root')
);

registerSW();

window.addEventListener('online', () => {
    store.dispatch(app.setOnline());
});
window.addEventListener('offline', () => {
    store.dispatch(app.setOffline());
});

if (module.hot) {
    // When any of the dependencies to this entry file change we should hot reload.
    module.hot.accept();
}
