import React from 'react';
import { shape, string } from 'prop-types';
import { MapPin as MapPinIcon } from 'react-feather';

import Icon from '@magento/venia-ui/lib/components/Icon';
import { mergeClasses } from '@magento/venia-ui/lib/classify';
import Select from '@magento/venia-ui/lib/components/Select';

import defaultClasses from './countryCurrencySwitcher.css';
import { Util } from '@magento/peregrine';



const { BrowserPersistence } = Util;

const COUNTRY_CURRENCY_SWITCHER_ICON = (
    <Icon
        src={MapPinIcon}
        attrs={{
            stroke: 'rgb(var(--venia-text))'
        }}
    />
);

const countryCurencydata = [
    {
        value: '',
        label: 'Select Country'
    },
    {
        value: 'USA-USD',
        label: 'USA-USD'
    },
    {
        value: 'FR-EUR',
        label: 'FR-EUR'
    },
    {
        value: 'ES-EUR',
        label: 'ES-EUR'
    },
    {
        value: 'DE-EUR',
        label: 'DE-EUR'
    },
    {
        value: 'PL-PLN',
        label: 'PL-PLN'
    },
    {   
        value: 'NO-NOK',
        label: 'NO-NOK'
    },
    {   
        value: 'DK-DKK',
        label: 'DK-DKK'
    },
    {   
        value: 'UK-GBP-uk',
        label: 'UK-GBP'
    }
];

const CountryCurrencySwitcher = props => {
    
    const classes = mergeClasses(defaultClasses, props.classes);
    const countryCurrencyIcon = COUNTRY_CURRENCY_SWITCHER_ICON;
    const storage = new BrowserPersistence();
    function handleChange(event) {
        let countryCurrency = event.target.value;
        countryCurrency = countryCurrency.split('-');
        
        storage.setItem('eswCountry', countryCurrency[0]);
        storage.setItem('currency', countryCurrency[1]);
        let store = '';
        if(typeof countryCurrency[2] !== 'undefined') {
            store = countryCurrency[2];
        }
        storage.setItem('store', store);
        window.location.reload(false);
        event.preventDefault();
      }
      const currentCountryCurrency = storage.getItem('eswCountry') + ' - ' + storage.getItem('currency');
    return (
        <div>
            <div>{currentCountryCurrency}</div>
        <Select
        field="quantity"
        items={countryCurencydata}
        onChange={handleChange}
    />
    </div>
    );
};

CountryCurrencySwitcher.propTypes = {
    classes: shape({
        root: string
    })
};

export default CountryCurrencySwitcher;
