import { useCallback, useEffect } from 'react';

import gql from 'graphql-tag';

import { mergeClasses } from '@magento/venia-ui/lib/classify';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { useCartContext } from '@magento/peregrine/lib/context/cart';
import { useHistory, useLocation } from 'react-router-dom';

const GET_ESHOPWORLD_CHECKOUT = gql`
    query getEShopWorldCheckout($cartId: String!) {
        eShopWorldCheckout(cartId: $cartId)
    }
`;

export const Checkout = props => {
    //const classes = mergeClasses(defaultClasses, props.classes);

    const [{ cartId }] = useCartContext();
    const [eswCheckout, { data, loading, error: fetchError }] = useLazyQuery(
        GET_ESHOPWORLD_CHECKOUT,
        {
            fetchPolicy: 'no-cache',
            onCompleted: data => {
                window.location.replace(data.eShopWorldCheckout);
            },
            onError: () => {}
        }
    );

    const handleCheckout = useCallback(() => {
        // async () => {
        eswCheckout({
            variables: {
                cartId
            }
        });
        // try {
        //    const tt = await eswCheckout({
        //         variables: {
        //             cartId
        //         }
        //     });
        //     const json = await tt;
        //     if(json) {
        //         alert(json)
        //     }
        // //   const checkout =  await data
        // //   alert(data)
        //     // console.log(checkout)
        //    // window.location.replace('')
        // } catch (err) {
        //     alert(err)
        //     console.error(err);
        // }
    }, [eswCheckout, cartId]);

    return {
        handleCheckout
    };
};
