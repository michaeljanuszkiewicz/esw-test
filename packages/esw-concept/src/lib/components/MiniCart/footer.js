import React, { Suspense } from 'react';
import { bool, number, object, shape, string } from 'prop-types';

import { mergeClasses } from '@magento/venia-ui/lib/classify';
import Checkout from '@magento/venia-ui/lib/components/Checkout';
import CheckoutButton from '@magento/venia-ui/lib/components/Checkout/checkoutButton';
import Button from '@magento/venia-ui/lib/components/Button';

import defaultClasses from '@magento/venia-ui/lib/components/MiniCart/footer.css';
import TotalsSummary from '@magento/venia-ui/lib/components/MiniCart/totalsSummary';
import { useHistory, useLocation } from 'react-router-dom';
import { useHeader } from '@magento/peregrine/lib/talons/MiniCart/useHeader';

import {Checkout as eswCheckout} from '../Esw/checkout';

const Footer = props => {

    const {
        currencyCode,
        isMiniCartMaskOpen,
        numItems,
        setStep,
        step,
        subtotal
    } = props;

    const classes = mergeClasses(defaultClasses, props.classes);
    const footerClassName = isMiniCartMaskOpen
        ? classes.root_open
        : classes.root;
    const placeholderButton = (
        <div className={classes.placeholderButton}>
            <CheckoutButton disabled={true} />
        </div>
    );
    const history = useHistory(); 

    function handleCart(event) {
       history.push("/cart");
      }

    const esw = eswCheckout();
    const {
        handleCheckout
    } = esw;

    return (
        <div className={footerClassName}>
            <TotalsSummary
                currencyCode={currencyCode}
                numItems={numItems}
                subtotal={subtotal}
            />
                       <div className={classes.footer}>
                <Button 
                onClick={handleCart}
                
                >Edit Cart</Button>
                <CheckoutButton
                    priority="high"
                    //disabled={isSubmitDisabled}
                    onClick={handleCheckout}
                >
                    Checkout
                </CheckoutButton>
            </div>
            
        </div>
    );
};

Footer.propTypes = {
    cart: object,
    classes: shape({
        placeholderButton: string,
        root: string,
        root_open: string,
        summary: string,
        footer: string
    }),
    currencyCode: string,
    isMiniCartMaskOpen: bool,
    numItems: number,
    subtotal: number
};

export default Footer;
